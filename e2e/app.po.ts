import { browser, element, by } from 'protractor';

export class TalentStackupPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('tas-root h1')).getText();
  }
}
