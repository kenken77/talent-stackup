import { TalentStackupPage } from './app.po';

describe('talent-stackup App', function() {
  let page: TalentStackupPage;

  beforeEach(() => {
    page = new TalentStackupPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('tas works!');
  });
});
