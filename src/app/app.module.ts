import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
// custom app components.
import { HeaderComponent } from './shared/header.component';
import { SignInComponent } from './unprotected/signin.component';
import { SignUpComponent } from './unprotected/signup.component';
import { CourseComponent } from './protected/course.component';
import { DashboardComponent } from './protected/dashboard.component';
import { StudentComponent } from './protected/student.component'; 

import { AuthGuard } from './shared/auth.guard';
import { AuthService } from './shared/auth.service';

import { routing } from './app.routing';

@NgModule({
    imports: [BrowserModule, HttpModule, routing, ReactiveFormsModule],
    exports: [],
    declarations: [HeaderComponent, 
    SignInComponent, 
    CourseComponent, 
    DashboardComponent, 
    StudentComponent],
    providers: [AuthGuard, AuthService],
    bootstrap: [AppComponent]
})

export class AppModule { }